package fr.gouv.justice.mtteditique;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = { "fr.gouv.justice" })
public class MttEditiqueApplication {

	public static void main(String[] args) {
		SpringApplication.run(MttEditiqueApplication.class, args);
	}

}

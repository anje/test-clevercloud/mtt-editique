package fr.gouv.justice.mtteditique.controller;

import fr.gouv.justice.mtteditique.dto.TicketDTO;
import fr.gouv.justice.mtteditique.dto.WorkflowDTO;
import fr.gouv.justice.mtteditique.service.ticket.ITicketService;
import fr.gouv.justice.mtteditique.utils.response.ResponseMessage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1")
public class TicketController {

    @Autowired
    private ITicketService ticketService;

    @Operation(summary = "API récupérant la liste des documents créés")
    @GetMapping(value = "/ticket/list")
    public ResponseEntity<List<TicketDTO>> getTickets(
            @RequestParam(name = "editor") String editor,
            @RequestParam(name = "project") String project,
            @RequestParam(name = "userId") Long userId,
            @RequestParam(name = "useCaseName") String useCaseName,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "30") int size) {

        Pageable paging = PageRequest.of(page, size);

        List<TicketDTO> ticketDTOS = ticketService.getTickets(editor, project, userId, useCaseName, paging);

        return ResponseEntity.ok().body(ticketDTOS);
    }

    @Operation(summary = "API supprimant un document")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Document supprimé"),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)})
    @DeleteMapping(value = "/ticket/delete/{id}")
    public ResponseEntity<Boolean> getTickets(@PathVariable Long id) {

        boolean isRemoved = ticketService.delete(id);

        return ResponseEntity.ok().body(isRemoved);
    }

    @Operation(summary = "API de callback mettant à jour un document")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Document mis à jour"),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)})
    @PostMapping(value = "/workflow/update",
            consumes = { MediaType.APPLICATION_JSON_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResponseMessage> workflowUpdate(@RequestBody WorkflowDTO workflowDTO) {

        boolean response = ticketService.updateWorkflow(workflowDTO);

        if (response) {
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Document status updated successfully"));
        }

        return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Could not update document missing data : " + workflowDTO));

    }

}

package fr.gouv.justice.mtteditique.controller;

import fr.gouv.justice.mtteditique.dto.UserDTO;
import fr.gouv.justice.mtteditique.service.user.IUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1")
public class UserController {

    @Autowired
    private IUserService userService;

    @Operation(summary = "API récupérant la liste des utilisateurs répartis par projet")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateurs récupérés"),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)})
    @GetMapping(value = "/users/{project}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<UserDTO>> getUsers(@PathVariable String project) {
        List<UserDTO> userDTOS = userService.getUserByProject(project.toUpperCase());

        if (userDTOS.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(userDTOS);
    }

    @Operation(summary = "API récupérant la liste des utilisateurs répartis par projet et par groupe")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Utilisateurs récupérés"),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)})
    @GetMapping(value = "/users/{editor}/{group}", produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<List<UserDTO>> getUsers(@PathVariable String editor, @PathVariable String group) {
        List<UserDTO> userDTOS = userService.getUserByEditorAndGroup(editor.toUpperCase(), group);

        if (userDTOS.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(userDTOS);
    }
}

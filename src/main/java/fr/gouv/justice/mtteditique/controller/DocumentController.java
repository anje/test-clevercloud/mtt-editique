package fr.gouv.justice.mtteditique.controller;

import fr.gouv.justice.mtteditique.dto.DocumentDTO;
import fr.gouv.justice.mtteditique.dto.TicketDTO;
import fr.gouv.justice.mtteditique.service.document.IDocumentService;
import fr.gouv.justice.mtteditique.service.ticket.ITicketService;
import fr.gouv.justice.mtteditique.utils.StatusEnum;
import fr.gouv.justice.mtteditique.utils.response.ResponseMessage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import java.nio.file.Files;
import java.nio.file.Path;
import org.springframework.core.io.Resource;

@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1")
public class DocumentController {

    @Autowired
    private IDocumentService documentService;

    @Autowired
    private ITicketService ticketService;

    @Operation(summary = "API callback sauvegardant des documents en base de donnée et système de fichier")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Document sauvegardé"),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)})
    @PostMapping(value = "/document/save/{documentId}",
            consumes = { MediaType.MULTIPART_FORM_DATA_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<ResponseMessage> saveDocument(
            @PathVariable String documentId,
            @RequestPart("files") MultipartFile[] files
    ) {

        if (documentId == null || files == null) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Error : documentId or files attribute is null"));
        }

        try {

            TicketDTO ticketDTO = ticketService.findTicketByDocumentId(documentId);

            for (MultipartFile file : files) {
                String fileName = file.getOriginalFilename();
                String extension = file.getContentType();

                if (extension != null) {
                    extension = extension.replace("//", "");
                }

                DocumentDTO documentDTO = new DocumentDTO();
                documentDTO.setFilename(fileName);
                documentDTO.setExtension(extension);
                documentDTO.setTicketId(ticketDTO.getId());
                documentDTO.setSize(file.getSize());

                DocumentDTO documentResponse = documentService.saveSplitExtension(documentDTO);

                if (documentResponse != null) {
                    documentService.writeFileToSystemDecodeBase64(documentResponse, file);
                } else {
                    return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Error in database !"));
                }
            }

            ticketDTO.setStatus(StatusEnum.TERMINER.getStatus());
            ticketService.save(ticketDTO);

            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage("Uploaded files successfully"));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage("Could not upload files : " + e.getMessage()));
        }
    }

    @Operation(summary = "API récupérant un document")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Document récupéré"),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)})
    @GetMapping(value = "/document/{id}")
    public ResponseEntity<Resource> getDocument(@PathVariable Long id) throws Exception {

        DocumentDTO documentDTOObject = documentService.findById(id);

        if (!documentService.getFile(documentDTOObject.getFilename())) {
            ResponseEntity.badRequest().body("Le document est introuvable !");
        }

        Resource file = documentService.download(documentDTOObject.getFilename());
        Path path = file.getFile().toPath();

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, Files.probeContentType(path))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }

}

package fr.gouv.justice.mtteditique.controller;

import fr.gouv.justice.mtteditique.dto.AuthRequestDTO;
import fr.gouv.justice.mtteditique.dto.DocumentSaveDTO;
import fr.gouv.justice.mtteditique.dto.WorkflowDTO;
import fr.gouv.justice.mtteditique.facade.IEditiqueServiceFacade;
import fr.gouv.justice.mtteditique.service.factory.EditiqueFactoryService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.Locale;


@CrossOrigin
@RestController
@RequestMapping(value = "/api/v1")
public class EditiqueController {

    @Autowired
    EditiqueFactoryService editiqueFactoryService;

    @Operation(summary = "Authentification d'un utilisateur")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "utilisateur connecté",
                    content = { @Content(mediaType = "application/json") }),
            @ApiResponse(responseCode = "500", description = "Internal server error",
                    content = @Content)})
    @PostMapping(value = "/authentication", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> authentication(@RequestBody @Valid AuthRequestDTO authDTO) {

        IEditiqueServiceFacade editiqueServiceFacade = editiqueFactoryService.getProvider(authDTO.getEditor());

        if (editiqueServiceFacade == null) {
            return ResponseEntity.badRequest().build();
        }

        String message = editiqueServiceFacade.authentication(authDTO.getUser(), authDTO.getProject());
        if (message == null) {
            return ResponseEntity.badRequest().body("Authentification échoué !");
        }

        return ResponseEntity.ok().body(message);
    }

    @Operation(summary = "[ Non utilisé ] | API de téléchargement d'un document")
    @PostMapping(value = "/{editor}/{project}/unitary/document/download",
            consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE },
            produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> unitaryDocumentDownload(@RequestHeader("Authorization") String token,
                                                          @PathVariable String editor,
                                                          @PathVariable String project,
                                                          @RequestBody String flux
    ) {
        IEditiqueServiceFacade editiqueServiceFacade = editiqueFactoryService.getProvider(editor.toUpperCase(Locale.ROOT));

        if (editiqueServiceFacade == null) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(editiqueServiceFacade.unitaryDocumentDownload(token, project, flux));
    }

    @Operation(summary = "API de création d'un document intéractif")
    @PostMapping(value = "/{editor}/{project}/{useCaseName}/{username}/{group}/interactive/document/edit",
            consumes = { MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE },
            produces = { MediaType.APPLICATION_JSON_VALUE })
    public ResponseEntity<String> interactivityDocumentEdit(@RequestHeader("Authorization") String token,
                                                            @PathVariable String editor,
                                                            @PathVariable String project,
                                                            @PathVariable String useCaseName,
                                                            @PathVariable String username,
                                                            @PathVariable Long group,
                                                            @RequestPart("file") MultipartFile file
    ) {
        IEditiqueServiceFacade editiqueServiceFacade = editiqueFactoryService.getProvider(editor.toUpperCase(Locale.ROOT));

        if (editiqueServiceFacade == null) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(editiqueServiceFacade.interactivityDocumentEdit(username, token, project, useCaseName, file, group));
    }

    @Operation(summary = "API pemettant de valider et terminer un document interactif")
    @PostMapping(value = "/{editor}/{project}/{group}/interactive/document/save", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> interactivityDocumentSave(@RequestHeader("Authorization") String token,
                                                            @PathVariable String editor,
                                                            @PathVariable String project,
                                                            @PathVariable Long group,
                                                            @RequestBody DocumentSaveDTO documentSaveDTO
    ) {
        IEditiqueServiceFacade editiqueServiceFacade = editiqueFactoryService.getProvider(editor.toUpperCase(Locale.ROOT));

        if (editiqueServiceFacade == null) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(editiqueServiceFacade.interactivityDocumentSave(token, documentSaveDTO, group));
    }

    @Operation(summary = "API de création d'un objet workflow de validation")
    @PostMapping(value = "/create/workflow", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Boolean> createWorkflow(@RequestHeader("Authorization") String token, @RequestBody WorkflowDTO workflowDTO) {
        IEditiqueServiceFacade editiqueServiceFacade = editiqueFactoryService.getProvider(workflowDTO.getEditor().toUpperCase(Locale.ROOT));

        if (editiqueServiceFacade == null) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(editiqueServiceFacade.createWorkflow(token, workflowDTO));
    }

    @Operation(summary = "API récupérant la liste des trames locales")
    @GetMapping(value = "/{editor}/{project}/templates", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> getTemplates(@RequestHeader("Authorization") String token,
                                               @PathVariable String editor,
                                               @PathVariable String project) {
        IEditiqueServiceFacade editiqueServiceFacade = editiqueFactoryService.getProvider(editor.toUpperCase(Locale.ROOT));

        if (editiqueServiceFacade == null) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().body(editiqueServiceFacade.getTemplates(token, project));
    }

}

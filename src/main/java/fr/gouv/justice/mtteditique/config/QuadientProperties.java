package fr.gouv.justice.mtteditique.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "provider.quadient.basic-auth")
@Data
public class QuadientProperties {

    private String username;

    private String password;

    private String endpoint;
}

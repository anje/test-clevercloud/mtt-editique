package fr.gouv.justice.mtteditique.config;

import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing
@EnableConfigurationProperties({OpenTextProperties.class, QuadientProperties.class})
public class AppConfig {

}

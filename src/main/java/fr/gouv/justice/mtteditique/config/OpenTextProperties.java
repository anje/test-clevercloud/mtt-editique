package fr.gouv.justice.mtteditique.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "provider.opentext")
@Data
public class OpenTextProperties {

    private String otdsUrl;

    private String otdsTenantPath;

    private String oath2ClientId;

    private String grantType;

    private String username;

    private String password;

    private String oauth2ClientId;

    private String clientId;

    private String scope;

    private String devProxy;

    private String exOrcUrl;

    private String exstreamDomain;

    private String urlCreateWorkflow;
}

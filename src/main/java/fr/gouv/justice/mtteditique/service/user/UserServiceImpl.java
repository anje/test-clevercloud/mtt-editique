package fr.gouv.justice.mtteditique.service.user;

import fr.gouv.justice.mtteditique.dto.UserDTO;
import fr.gouv.justice.mtteditique.mapper.UserMapper;
import fr.gouv.justice.mtteditique.model.User;
import fr.gouv.justice.mtteditique.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements IUserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<UserDTO> getUserByProject(String project) {
        Optional<List<User>> users = userRepository.findByProject(project);

        return userMapper.toDTOs(users.get());
    }

    @Override
    public UserDTO getUserById(Long id) {
        Optional<User> user = userRepository.findById(id);

        return userMapper.toDTO(user.get());
    }

    @Override
    public List<UserDTO> getUserByEditorAndGroup(String editor, String group) {
        Optional<List<User>> users = userRepository.findUserByEditorAndGroup(editor, group);

        return userMapper.toDTOs(users.get());
    }
}

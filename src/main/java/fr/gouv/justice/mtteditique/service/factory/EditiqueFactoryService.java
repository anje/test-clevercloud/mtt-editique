package fr.gouv.justice.mtteditique.service.factory;

import fr.gouv.justice.mtteditique.facade.EditiqueServiceFacadeBaseAbstract;
import fr.gouv.justice.mtteditique.facade.IEditiqueServiceFacade;
import fr.gouv.justice.mtteditique.service.opentext.DocOpenText;
import fr.gouv.justice.mtteditique.service.quadient.DocQuadient;
import fr.gouv.justice.mtteditique.utils.ProviderEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EditiqueFactoryService implements IEditiqueFactoryService {

    @Autowired
    private DocOpenText docOpenText;

    @Autowired
    private DocQuadient docQuadient;

    @Override
    public EditiqueServiceFacadeBaseAbstract getOpenTextProvider() {
         return docOpenText;
    }

    @Override
    public EditiqueServiceFacadeBaseAbstract getQuandientProvider() {
        return docQuadient;
    }

    public IEditiqueServiceFacade getProvider(String provider) {

        if (ProviderEnum.OPENTEXT.getProvider().equalsIgnoreCase(provider.toUpperCase())) {
            return getOpenTextProvider();
        } else if (ProviderEnum.QUADIENT.getProvider().equalsIgnoreCase(provider.toUpperCase())) {
            return getQuandientProvider();
        }

        return null;
    }
}

package fr.gouv.justice.mtteditique.service.user;

import fr.gouv.justice.mtteditique.dto.UserDTO;

import java.util.List;

public interface IUserService {

    List<UserDTO> getUserByProject(String project);

    List<UserDTO> getUserByEditorAndGroup(String editor, String group);

    UserDTO getUserById(Long id);
}

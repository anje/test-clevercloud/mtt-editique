package fr.gouv.justice.mtteditique.service.ticket;

import fr.gouv.justice.mtteditique.dto.TicketDTO;
import fr.gouv.justice.mtteditique.dto.WorkflowDTO;
import fr.gouv.justice.mtteditique.mapper.TicketMapper;
import fr.gouv.justice.mtteditique.model.Ticket;
import fr.gouv.justice.mtteditique.model.User;
import fr.gouv.justice.mtteditique.repository.TicketRepository;
import fr.gouv.justice.mtteditique.repository.UserRepository;
import fr.gouv.justice.mtteditique.service.document.IDocumentService;
import fr.gouv.justice.mtteditique.utils.StatusEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TicketServiceImpl implements ITicketService{

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TicketRepository ticketRepository;

    @Autowired
    private TicketMapper ticketMapper;

    @Autowired
    private IDocumentService documentService;

    @Override
    public List<TicketDTO> getTickets(String editor, String project, Long userId, String useCaseName, Pageable pageable) {

        useCaseName = useCaseName.toUpperCase();
        String[] parts = useCaseName.split(",");

        Page<Ticket> page = ticketRepository.getTickets(editor.toUpperCase(), project.toUpperCase(), userId, parts, pageable);

        return ticketMapper.toDTOs(page.getContent());
    }

    @Override
    public TicketDTO save(TicketDTO ticketDTO) {
        Ticket ticket = ticketRepository.save(ticketMapper.toEntity(ticketDTO));

        return ticketMapper.toDTO(ticket);
    }

    @Override
    public boolean delete(Long id) {
        Optional<Ticket> ticket = ticketRepository.findById(id);

        ticket.ifPresent(value -> {

            if (value.getDocuments() != null) {
                value.getDocuments().forEach(d -> {

                    documentService.deleteFileFromSystem(d.getFilename());

                });
            }
            ticketRepository.delete(value);
        });

        return true;
    }

    @Override
    public TicketDTO create(String username, String token, String editor, String project, String useCaseName, String documentId, String url) {

        Optional<User> user = userRepository.findUserByUsernameAndProjectAndEditor(username, project.toUpperCase(), editor);

        Ticket ticket = new Ticket();
        ticket.setDocumentId(documentId);
        ticket.setUseCaseName(useCaseName.toUpperCase());
        ticket.setUrl(url);
        ticket.setStatus(StatusEnum.BROUILLON.getStatus());
        ticket.setEditor(editor.toUpperCase());
        ticket.setProject(project.toUpperCase());

        if (user.isPresent()) {
            ticket.setUser(user.get());
            ticket.setValideur(user.get());
        }

        ticketRepository.save(ticket);

        return ticketMapper.toDTO(ticket);
    }

    @Override
    public TicketDTO findTicketByDocumentId(String documentId) {
        Optional<Ticket> ticket = ticketRepository.findTicketByDocumentId(documentId);

        return ticketMapper.toDTO(ticket.get());
    }

    @Override
    public boolean updateWorkflow(WorkflowDTO workflowDTO) {
        Optional<Ticket> ticket = ticketRepository.findTicketByDocumentId(workflowDTO.getDocumentID());

        if (ticket.isPresent()) {
            Ticket data = ticket.get();

            Optional<User> user = userRepository.findUserByUsernameAndProjectAndEditor(workflowDTO.getValideur(), data.getProject().toUpperCase(), data.getEditor().toUpperCase());

            data.setStatus(workflowDTO.getStatus());

            if (user.isPresent()) {
                data.setValideur(user.get());
            }

            ticketRepository.save(data);

            return true;
        }
        return false;
    }
}

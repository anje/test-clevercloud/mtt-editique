package fr.gouv.justice.mtteditique.service.ticket;

import fr.gouv.justice.mtteditique.dto.TicketDTO;
import fr.gouv.justice.mtteditique.dto.WorkflowDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ITicketService {

    List<TicketDTO> getTickets(String editor, String project, Long userId, String useCaseName, Pageable pageable);

    TicketDTO create(String username, String token, String editor, String project, String useCaseName, String documentId, String url);

    TicketDTO save(TicketDTO ticketDTO);

    boolean delete(Long id);

    TicketDTO findTicketByDocumentId(String documentId);

    boolean updateWorkflow(WorkflowDTO workflowDTO);

}

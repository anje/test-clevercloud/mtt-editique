package fr.gouv.justice.mtteditique.service.document;

import fr.gouv.justice.mtteditique.dto.DocumentDTO;
import fr.gouv.justice.mtteditique.dto.DocumentWithContentDTO;
import fr.gouv.justice.mtteditique.mapper.DocumentMapper;
import fr.gouv.justice.mtteditique.model.Document;
import fr.gouv.justice.mtteditique.repository.DocumentRepository;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Optional;
import java.util.UUID;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.web.multipart.MultipartFile;


@Service
@Transactional
public class DocumentServiceImpl implements IDocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    @Autowired
    private DocumentMapper documentMapper;

    String filesPath = "./tmp";

    @Override
    public DocumentDTO findById(Long id) throws ObjectNotFoundException {
        Optional<Document> document = documentRepository.findById(id);

        if (!document.isPresent()) {
            throw new ObjectNotFoundException(Document.class, "Unable to locate document with id: " + id);
        }

        return documentMapper.toDTO(document.get());
    }

    @Override
    public DocumentDTO save(DocumentDTO documentDTO) {

        documentDTO.setFilename(generateFileName(documentDTO.getFilename(), documentDTO.getExtension()));

        Document document = documentRepository.save(documentMapper.toEntity(documentDTO));

        return documentMapper.toDTO(document);
    }

    @Override
    public Resource download(String filename) {

        try {
            Path file = Paths.get(filesPath)
                    .resolve(filename);
            Resource resource = new UrlResource(file.toUri());

            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("Could not read the file!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error: " + e.getMessage());
        }
    }

    @Override
    public void writeBase64FileToSystem(DocumentWithContentDTO documentWithContentDTO) throws Exception {

        byte[] decodedBytes = Base64.getDecoder().decode(documentWithContentDTO.getContent().getBytes(StandardCharsets.UTF_8));

        Path path = Paths.get(filesPath, documentWithContentDTO.getFilename());

        try {
            Files.write(path, decodedBytes);
        } catch(Exception e) {
            throw new Exception("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public void writeFileToSystem(DocumentDTO documentResponse, MultipartFile file) {
        try {
            Path path = Paths.get(filesPath, documentResponse.getFilename());
            Files.write(path, file.getBytes());
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public void writeFileToSystemDecodeBase64(DocumentDTO documentResponse, MultipartFile file) {
        try {
            Path path = Paths.get(filesPath, documentResponse.getFilename());
            byte[] decodedBytes = Base64.getDecoder().decode(file.getBytes());

            Files.write(path, decodedBytes);
        } catch (Exception e) {
            throw new RuntimeException("Could not store the file. Error: " + e.getMessage());
        }
    }

    @Override
    public boolean getFile(String filename) {

        if (Files.exists(Paths.get(filesPath+"/"+filename))) {
            return true;
        }

        return false;
    }

    @Override
    public boolean deleteFileFromSystem(String filename) {

        if ( getFile(filename) ) {
            Path path = Paths.get(filesPath, filename);

            try {
                Files.delete(path);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return false;
    }

    public String generateFileName(String filename, String extension) {
        return filename + '_' + generateRandomUUID() + getExtension(extension);
    }

    public String getExtension(String extension) {
        if (extension.equals("application/pdf") || extension.equals("text/pdf")) {
            return ".pdf";
        } else if (extension.equals("application/xml") || extension.equals("text/xml")) {
            return ".xml";
        }

        return "";
    }

    public String generateRandomUUID() {
        String uuid = UUID.randomUUID().toString();

        return uuid.substring(uuid.length() - 16);
    }

    @Override
    public DocumentDTO saveSplitExtension(DocumentDTO documentDTO) {
        documentDTO.setFilename(generateFileNameSplitExtension(documentDTO.getFilename()));

        Document document = documentRepository.save(documentMapper.toEntity(documentDTO));

        return documentMapper.toDTO(document);
    }

    public String generateFileNameSplitExtension(String filename) {

        String[] fn = filename.split("[.]", 0);

        return fn[0] + '_' + generateRandomUUID() + "." + fn[1];
    }
}

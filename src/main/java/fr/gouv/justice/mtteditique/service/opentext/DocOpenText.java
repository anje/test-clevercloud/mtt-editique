package fr.gouv.justice.mtteditique.service.opentext;

import fr.gouv.justice.mtteditique.config.OpenTextProperties;
import fr.gouv.justice.mtteditique.dto.DocumentDTO;
import fr.gouv.justice.mtteditique.dto.DocumentSaveDTO;
import fr.gouv.justice.mtteditique.dto.DocumentWithContentDTO;
import fr.gouv.justice.mtteditique.dto.TicketDTO;
import fr.gouv.justice.mtteditique.dto.WorkflowDTO;
import fr.gouv.justice.mtteditique.facade.EditiqueServiceFacadeBaseAbstract;
import fr.gouv.justice.mtteditique.mapper.TicketMapper;
import fr.gouv.justice.mtteditique.model.User;
import fr.gouv.justice.mtteditique.repository.TicketRepository;
import fr.gouv.justice.mtteditique.repository.UserRepository;
import fr.gouv.justice.mtteditique.service.document.IDocumentService;
import fr.gouv.justice.mtteditique.service.ticket.ITicketService;
import fr.gouv.justice.mtteditique.utils.ProjectEnum;
import fr.gouv.justice.mtteditique.utils.StatusEnum;
import fr.gouv.justice.mtteditique.utils.UseCaseNameEnum;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.Base64;
import java.util.Locale;
import java.util.Optional;

@Component
public class DocOpenText extends EditiqueServiceFacadeBaseAbstract {

    @Autowired
    OpenTextProperties openTextProperties;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    IDocumentService documentService;

    @Autowired
    ITicketService ticketService;

    @Autowired
    TicketRepository ticketRepository;

    @Autowired
    TicketMapper ticketMapper;

    @Autowired
    private UserRepository userRepository;

    public DocOpenText() {

    }

    @Override
    public String authentication(String username, String project) {
        String url = openTextProperties.getOtdsUrl()+"/otdsws"+openTextProperties.getOtdsTenantPath()+getTenantAuthentication(project) +"/oauth2/token";

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
        headers.add("Accept", "*/*");
        headers.add("Connection", "keep-alive");

        MultiValueMap<String, String> bodyValues = new LinkedMultiValueMap<>();
        bodyValues.add("grant_type", openTextProperties.getGrantType());
        bodyValues.add("username", openTextProperties.getUsername());
        bodyValues.add("password", openTextProperties.getPassword());
        bodyValues.add("client_id", openTextProperties.getOath2ClientId());
        bodyValues.add("scope", openTextProperties.getScope());

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(bodyValues, headers);

        ResponseEntity<String> response = restTemplate
                .exchange(url, HttpMethod.POST, entity, String.class);

        return response.getBody();
    }

    @Override
    public String unitaryDocumentDownload(String token, String project, String flux) {
        String url = openTextProperties.getExOrcUrl()+"/api/v1/inputs/ondemand/"+openTextProperties.getExstreamDomain()+"/TP_HTTPSSERVICE";

        HttpHeaders headers = getHeader(token, "application/xml; charset=utf-8");

        HttpEntity<String> entity = new HttpEntity<>(flux, headers);

        ResponseEntity<String> response = restTemplate
                .exchange(url, HttpMethod.POST, entity, String.class);

        return response.getBody();
    }

    @Override
    public String interactivityDocumentEdit(String username, String token, String project, String useCaseName, MultipartFile file, Long group) {
        String url = openTextProperties.getExOrcUrl()+"/api/v1/inputs/ondemand/"+getUseCaseServices(useCaseName, group)[3]+"/"+getUseCaseServices(useCaseName, group)[0];
        String iframeUrl;

        HttpHeaders headers = getHeader(token, file.getContentType());

        JSONParser parser = new JSONParser();

        try {

            Resource multiPartFile = new ByteArrayResource(file.getBytes()){
                @Override
                public String getFilename(){
                    return file.getOriginalFilename();
                }
            };

            HttpEntity<Resource> entity = new HttpEntity<>(multiPartFile, headers);
            ResponseEntity<String> response = restTemplate
                    .exchange(url, HttpMethod.POST, entity, String.class);

            //

            JSONObject json = (JSONObject) parser.parse(response.getBody());
            JSONArray object = (JSONArray) json.get("data");
            JSONObject content = (JSONObject) object.get(0);

            byte[] decodedBytes = Base64.getDecoder().decode(content.get("content").toString());
            String documentString = new String(decodedBytes);

            JSONObject document = (JSONObject) parser.parse(documentString);

            JSONObject jsonObject = new JSONObject();

            iframeUrl = "https://poc-mdljcn.eimdemo.com/empower/resource/docedit/"+document.get("documentId")+"/open?showUserLogs=true&tenant="+getUseCaseServices(useCaseName, group)[2];
            // iframeUrl = "https://vm-rdbeercn.eimdemo.com/empower/resource/docedit/"+document.get("documentId")+"/open?showUserLogs=true&tenant="+getUseCaseServices(useCaseName)[2];

            // Create a ticket
            ticketService.create(username, token, "OPENTEXT", project, useCaseName, document.get("documentId").toString(), iframeUrl);

            // Response JSON
            jsonObject.put("documentId",document.get("documentId"));
            jsonObject.put("url", iframeUrl);

            return jsonObject.toJSONString();

        } catch (ParseException | IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Boolean interactivityDocumentSave(String token, DocumentSaveDTO documentSaveDTO, Long group) {
        String url = openTextProperties.getExOrcUrl()+"/api/v1/inputs/fulfillment/ondemand/"+getUseCaseServices(documentSaveDTO.getUseCaseName(), group)[3]+"/"+getUseCaseServices(documentSaveDTO.getUseCaseName(), group)[1];

        HttpHeaders headers = getHeader(token, MediaType.APPLICATION_JSON_VALUE);

        JSONObject jsonObject = new JSONObject();
        JSONArray array = new JSONArray();
        array.add(documentSaveDTO.getDocumentId());
        jsonObject.put("documents",array);

        HttpEntity<?> entity = new HttpEntity<>(jsonObject, headers);

        ResponseEntity<String> response = restTemplate
                .exchange(url, HttpMethod.POST, entity, String.class);

        if ( response.getBody() != null) {
            saveDocument(response.getBody(), documentSaveDTO);
        }


        return true;
    }

    public void saveDocument(String response, DocumentSaveDTO documentSaveDTO) {
        JSONParser parser = new JSONParser();
        try {
            JSONObject json = (JSONObject) parser.parse(response);
            JSONArray object = (JSONArray) json.get("data");
            object.forEach(it -> {
                JSONObject content = (JSONObject) it;

                if (!content.get("mimeType").toString().equals("application/vnd.exstream-empower")) {

                    TicketDTO ticketDTO = ticketService.findTicketByDocumentId(documentSaveDTO.getDocumentId());

                    // Enregistrement du document en BDD
                    DocumentDTO documentDTO = new DocumentDTO();
                    documentDTO.setFilename(documentSaveDTO.getFilename());
                    documentDTO.setExtension(content.get("mimeType").toString());
                    documentDTO.setTicketId(ticketDTO.getId());
                    documentDTO = documentService.save(documentDTO);

                    // Enregistrement du document en FilsSystem
                    DocumentWithContentDTO documentWithContentDTO = new DocumentWithContentDTO();
                    documentWithContentDTO.setFilename(documentDTO.getFilename());
                    documentWithContentDTO.setContent(content.get("content").toString());

                    try {
                        documentService.writeBase64FileToSystem(documentWithContentDTO);

                        ticketDTO.setStatus(StatusEnum.TERMINER.getStatus());

                        ticketRepository.save(ticketMapper.toEntity(ticketDTO));

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    /**
     * Récupération du tenant pour connexion
     * @param project
     * @return
     */
    public String getTenantAuthentication(String project) {
        ProjectEnum pjt = ProjectEnum.valueOf(project.toUpperCase(Locale.ROOT));

        switch (pjt) {
            case PRISME: return "entite1";
            case PPN: return "entite2";
            case CASSIOPEE: return "entite3";
            case PORTALIS: return "entite4";
            default: return "";
        }
    }

    /**
     * Récupération du nom du service par cas d'usage
     *
     * @param useCaseName
     * @param group
     * @return
     */
    public String[] getUseCaseServices(String useCaseName, Long group) {

        UseCaseNameEnum useCaseNameEnum = UseCaseNameEnum.valueOf(useCaseName.toUpperCase(Locale.ROOT));
        String[] services = new String[] {};

        switch (useCaseNameEnum) {
            case OPENTEXT_PRISME_JUGEMENT_NATIONALE_1:
            case OPENTEXT_PRISME_JUGEMENT_NATIONALE_2:
                services = new String[] {"PRISME1_HTTPSSERVICE", "PRISME1_FULF_HTTPSSERVICE", "entite1", "Test"};
                break;
            case OPENTEXT_PRISME_JUGEMENT_1:
                if (group == 1) {
                    services = new String[] {"PRISM1_CA_HTTPSSERVICE", "PRISM1_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_1"};
                } else if (group == 2) {
                    services = new String[] {"PRISM2_CA_HTTPSSERVICE", "PRISM2_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_2"};
                } else if (group == 3) {
                    services = new String[] {"PRISM3_CA_HTTPSSERVICE", "PRISM3_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_3"};
                } else if (group == 4) {
                    services = new String[] {"PRISM4_CA_HTTPSSERVICE", "PRISM4_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_4"};
                } else if (group == 5) {
                    services = new String[] {"PRISM5_CA_HTTPSSERVICE", "PRISM5_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_5"};
                } else {
                    services = new String[] {"PRISME1_HTTPSSERVICE", "PRISME1_FULF_HTTPSSERVICE", "entite1", "Test"};
                }
                break;
            case OPENTEXT_PRISME_JUGEMENT_2:
                if (group == 1) {
                    services = new String[] {"PRISM1_CA_HTTPSSERVICE", "PRISM1_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_1"};
                } else if (group == 2) {
                    services = new String[] {"PRISM2_CA_HTTPSSERVICE", "PRISM2_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_2"};
                } else if (group == 3) {
                    services = new String[] {"PRISM3_CA_HTTPSSERVICE", "PRISM3_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_3"};
                } else if (group == 4) {
                    services = new String[] {"PRISM4_CA_HTTPSSERVICE", "PRISM4_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_4"};
                } else if (group == 5) {
                    services = new String[] {"PRISM5_CA_HTTPSSERVICE", "PRISM5_CA_FULF_HTTPSSERVICE", "entite1", "Prisme_5"};
                } else {
                    services = new String[] {"PRISME1_HTTPSSERVICE", "PRISME1_FULF_HTTPSSERVICE", "entite1", "Test"};
                }
                break;
            case OPENTEXT_PPN_SOIT_TRANSMIS:
                services = new String[] {"ST_HTTPSSERVICE", "ST_FULF_HTTPSSERVICE", "entite2", "Test"};
                break;
            case OPENTEXT_CASSIOPEE_IPC_WITHOUT_INTERVIEW:
                services = new String[] {"PPN_IPC_HTTPSSERVICE", "PPN_IPC_FULF_HTTPSSERVICE", "entite3", "Test"};
                break;
            case OPENTEXT_CASSIOPEE_IPC_TRAME_LOCAL:
                if (group == 6) {
                    services = new String[] {"IPC1_CA_HTTPSSERVICE", "IPC1_CA_FULF_HTTPSSERVICE", "entite3", "K6OP1"};
                } else if (group == 7) {
                    services = new String[] {"IPC2_CA_HTTPSSERVICE", "IPC2_CA_FULF_HTTPSSERVICE", "entite3", "K6OP2"};
                } else if (group == 8) {
                    services = new String[] {"IPC3_CA_HTTPSSERVICE", "IPC3_CA_FULF_HTTPSSERVICE", "entite3", "K6OP3"};
                } else if (group == 9) {
                    services = new String[] {"IPC4_CA_HTTPSSERVICE", "IPC4_CA_FULF_HTTPSSERVICE", "entite3", "K6OP4"};
                } else if (group == 10) {
                    services = new String[] {"IPC5_CA_HTTPSSERVICE", "IPC5_CA_FULF_HTTPSSERVICE", "entite3", "K6OP5"};
                } else {
                    services = new String[] {"PPN_IPC_HTTPSSERVICE", "PPN_IPC_FULF_HTTPSSERVICE", "entite3", "Test"};
                }
                break;
            case OPENTEXT_CASSIOPEE_IPC_WITH_INTERVIEW:
                services = new String[] {"IPC_INT_HTTPSSERVICE", "IPC_INT_FULF_HTTPSSERVICE", "entite3", "Test"};
                break;
            case OPENTEXT_CASSIOPEE_JGT_CAS1_EDITABLE:
            case OPENTEXT_CASSIOPEE_JGT_CAS2:
            case OPENTEXT_CASSIOPEE_JGT_CAS3:
            case OPENTEXT_CASSIOPEE_JGT_CAS4:
                services = new String[] {"JUGEMENT_HTTPSSERVICE", "JUGEMENT_FULF_HTTPSSERVICE", "entite3", "Test"};
                break;
            case OPENTEXT_PORTALIS_DECISION_WITHOUT_INTERVIEW:
                services = new String[] {"PORTA_DECI_HTTPSSERVICE", "PORTA_DECI_FULF_HTTPSSERVICE", "entite4", "Test"};
                break;
            case OPENTEXT_PORTALIS_DECISION_WITH_INTERVIEW:
                services = new String[] {"PORTA_DECI_INT_HTTPSSERVICE", "PORTA_DECI_INT_FULF_HTTPSSERVICE", "entite4", "Test"};
                break;
            case OPENTEXT_PORTALIS_CONVOCATION_MULTI_NATIONALE:
            case OPENTEXT_PORTALIS_CONVOCATION_MONO_NATIONALE:
                services = new String[] {"PORTA_CONV_HTTPSSERVICE", "PORTA_CONV_FULF_HTTPSSERVICE", "entite4", "Test"};
                break;
            case OPENTEXT_PORTALIS_CONVOCATION_MULTI:
            case OPENTEXT_PORTALIS_CONVOCATION_MONO:
                if (group == 11) {
                    services = new String[] {"PORT_CA1_CONV_HTTPSSERVICE", "PORT_CA1_CONV_FULF_HTTPSSERVICE", "entite4", "PORTALIS1"};
                } else if (group == 12) {
                    services = new String[] {"PORT_CA2_CONV_HTTPSSERVICE", "PORT_CA2_CONV_FULF_HTTPSSERVICE", "entite4", "PORTALIS2"};
                } else if (group == 13) {
                    services = new String[] {"PORT_CA3_CONV_HTTPSSERVICE", "PORT_CA3_CONV_FULF_HTTPSSERVICE", "entite4", "PORTALIS3"};
                } else if (group == 14) {
                    services = new String[] {"PORT_CA4_CONV_HTTPSSERVICE", "PORT_CA4_CONV_FULF_HTTPSSERVICE", "entite4", "PORTALIS4"};
                } else if (group == 15) {
                    services = new String[] {"PORT_CA5_CONV_HTTPSSERVICE", "PORT_CA5_CONV_FULF_HTTPSSERVICE", "entite4", "PORTALIS5"};
                } else {
                    services = new String[] {"PORTA_CONV_HTTPSSERVICE", "PORTA_CONV_FULF_HTTPSSERVICE", "entite4", "Test"};
                }
                break;
            default:
                break;
        }

        return services;
    }

    @Override
    public Boolean createWorkflow(String token, WorkflowDTO workflowDTO) {
        String urlCreateWorkflow = openTextProperties.getUrlCreateWorkflow();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);

        JSONObject jsonObject1 = new JSONObject();

        JSONObject jsonObject2 = new JSONObject();
        jsonObject2.put("D_APPNAME", workflowDTO.getAppname().toUpperCase());
        jsonObject2.put("DocumentID", workflowDTO.getDocumentID());
        jsonObject2.put("D_PRIO", workflowDTO.getPriority());
        jsonObject2.put("D_CREATEUR", workflowDTO.getCreateur());
        jsonObject2.put("D_VALIDEUR", workflowDTO.getValideur());
        jsonObject2.put("D_DOC_NAME", workflowDTO.getDocName());
        jsonObject2.put("D_DATE_ECH", workflowDTO.getDateEcheance());

        jsonObject1.put("Properties", jsonObject2);

        HttpEntity<?> entity = new HttpEntity<>(jsonObject1, headers);

        ResponseEntity<String> response = restTemplate
                .exchange(urlCreateWorkflow, HttpMethod.POST, entity, String.class);

        if (response.getBody() != null) {

            Optional<User> userValideur = userRepository.findUserByUsernameAndProjectAndEditor(workflowDTO.getValideur(), workflowDTO.getAppname(), workflowDTO.getEditor());
            TicketDTO ticketDTO = ticketService.findTicketByDocumentId(workflowDTO.getDocumentID());

            if (userValideur.isPresent() && ticketDTO != null) {
                String opUrlWorkflow = "http://poc-pmoraisi2.eastus.cloudapp.azure.com:81/home/system/app/start/web/item/00155D940425A1EC985C0CDD987057D9."+response.getBody()+"/00155D940425A1ECA433127DDFFA57E0";
                ticketDTO.setValideurId(userValideur.get().getId());
                ticketDTO.setOpUrlWorkflow(opUrlWorkflow);
                ticketDTO.setStatus(StatusEnum.A_VALIDER.getStatus());

                ticketRepository.save(ticketMapper.toEntity(ticketDTO));
            }

            return true;
        }

        return false;
    }

    @Override
    public String getTemplates(String token, String project) {
        return null;
    }
}

package fr.gouv.justice.mtteditique.service.quadient;

import fr.gouv.justice.mtteditique.config.QuadientProperties;
import fr.gouv.justice.mtteditique.dto.DocumentSaveDTO;
import fr.gouv.justice.mtteditique.dto.WorkflowDTO;
import fr.gouv.justice.mtteditique.facade.EditiqueServiceFacadeBaseAbstract;
import fr.gouv.justice.mtteditique.repository.UserRepository;
import fr.gouv.justice.mtteditique.service.ticket.ITicketService;
import fr.gouv.justice.mtteditique.utils.UseCaseNameEnum;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.Locale;

@Component
public class DocQuadient extends EditiqueServiceFacadeBaseAbstract {

    @Autowired
    QuadientProperties quadientProperties;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ITicketService ticketService;

    @Autowired
    private UserRepository userRepository;

    public DocQuadient() {

    }

    @Override
    public String authentication(String username, String project) {
        String authStr = quadientProperties.getUsername()+":"+quadientProperties.getPassword();
        String base64Creds = Base64.getEncoder().encodeToString(authStr.getBytes());
        JSONObject cred = new JSONObject();
        cred.put("access_token", base64Creds);

        return cred.toJSONString();
    }

    @Override
    public String unitaryDocumentDownload(String token, String useCaseName, String flux) {
        return null;
    }

    @Override
    public String interactivityDocumentEdit(String username, String token, String project, String useCaseName, MultipartFile file, Long group) {
        String url = quadientProperties.getEndpoint() + getUseCaseService(useCaseName) + "/?user=" + username.replaceAll("@", "/");

        HttpHeaders headers = getHeader(token, file.getContentType());

        try {

            Resource multiPartFile = new ByteArrayResource(file.getBytes()){
                @Override
                public String getFilename(){
                    return file.getOriginalFilename();
                }
            };

            HttpEntity<Resource> entity = new HttpEntity<>(multiPartFile, headers);

            ResponseEntity<String> response = restTemplate
                    .exchange(url, HttpMethod.POST, entity, String.class);

            // Create a ticket
            try {
                JSONParser parser = new JSONParser();
                JSONObject json = (JSONObject) parser.parse(response.getBody());
                String iframeUrl = json.get("ticketUrl").toString();
                String documentIdFromUrl;

                if (useCaseName.toUpperCase().equals(UseCaseNameEnum.QUADIENT_PPN_SOIT_TRANSMIS_FORMULAIRE.getUseCaseName())) {
                    documentIdFromUrl = iframeUrl.substring(iframeUrl.indexOf("?interactive-process-ticket-id=") + 31);
                } else {
                    documentIdFromUrl = iframeUrl.substring(iframeUrl.indexOf("?id=") + 4);
                }

                ticketService.create(username, token, "QUADIENT", project, useCaseName, documentIdFromUrl, json.get("ticketUrl").toString());

            } catch (ParseException e) {
                e.printStackTrace();
            }

            return response.getBody();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Boolean interactivityDocumentSave(String token, DocumentSaveDTO documentSaveDTO, Long group) {
        return null;
    }


    /**
     * Récupération du service à appeler selon le cas d'usage
     *
     * @param useCaseName
     * @return
     */
    public String getUseCaseService(String useCaseName) {
        UseCaseNameEnum ucn = UseCaseNameEnum.valueOf(useCaseName.toUpperCase(Locale.ROOT));

        switch (ucn) {
            case QUADIENT_PRISME_JUGEMENT: return "prisme";
            case QUADIENT_PPN_SOIT_TRANSMIS: return "ppn";
            case QUADIENT_PORTALIS_DECISION: return "portalisDecision";
            case QUADIENT_PORTALIS_CONVOCATION_UNIQUE: return "portalisConvocationMono";
            case QUADIENT_PORTALIS_CONVOCATION_MULTI: return "portalisConvocationMulti";
            case QUADIENT_CASSIOPEE_IPC: return "cassiopeeIpc";
            case QUADIENT_PPN_SOIT_TRANSMIS_FORMULAIRE: return "ppnForm";
            case QUADIENT_CASSIOPEE_JGT_CAS1_EDITABLE: return "cassiopeeJugement";
            case QUADIENT_CASSIOPEE_JGT_CAS2: return "cassiopeeJugement";
            case QUADIENT_CASSIOPEE_JGT_CAS3: return "cassiopeeJugement";
            case QUADIENT_CASSIOPEE_JGT_CAS4: return "cassiopeeJugement";

            default: return "";
        }
    }

    @Override
    public Boolean createWorkflow(String token, WorkflowDTO workflowDTO) {
        return null;
    }

    @Override
    public String getTemplates(String token, String project) {

        String url = quadientProperties.getEndpoint() + "/listOfTemplates?casUsage=" + project.toLowerCase();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", token);

        HttpEntity<String> entity = new HttpEntity<>(null, headers);

        ResponseEntity<String> response = restTemplate
                .exchange(url, HttpMethod.GET, entity, String.class);

        return response.getBody();
    }
}

package fr.gouv.justice.mtteditique.service.document;

import fr.gouv.justice.mtteditique.dto.DocumentDTO;
import fr.gouv.justice.mtteditique.dto.DocumentWithContentDTO;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface IDocumentService {

    DocumentDTO findById(Long id);

    DocumentDTO save(DocumentDTO documentDTO);

    DocumentDTO saveSplitExtension(DocumentDTO documentDTO);

    Resource download(String filename);

    void writeBase64FileToSystem(DocumentWithContentDTO documentWithContentDTO) throws Exception;

    void writeFileToSystem(DocumentDTO documentResponse, MultipartFile file);

    void writeFileToSystemDecodeBase64(DocumentDTO documentResponse, MultipartFile file);

    boolean getFile(String filename);

    boolean deleteFileFromSystem(String filename);
}

package fr.gouv.justice.mtteditique.service.factory;

import fr.gouv.justice.mtteditique.facade.EditiqueServiceFacadeBaseAbstract;

public interface IEditiqueFactoryService {

    EditiqueServiceFacadeBaseAbstract getOpenTextProvider();

    EditiqueServiceFacadeBaseAbstract getQuandientProvider();

}

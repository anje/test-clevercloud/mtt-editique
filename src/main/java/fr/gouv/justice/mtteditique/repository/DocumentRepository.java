package fr.gouv.justice.mtteditique.repository;

import fr.gouv.justice.mtteditique.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepository extends JpaRepository<Document, Long>  {


}

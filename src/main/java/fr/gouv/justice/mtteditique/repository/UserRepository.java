package fr.gouv.justice.mtteditique.repository;

import fr.gouv.justice.mtteditique.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query("SELECT u FROM User u WHERE u.project like :project")
    Optional<List<User>> findByProject(@Param("project") String project);

    Optional<User> findUserByUsernameAndProjectAndEditor(String userName, String project, String editor);

    Optional<User> findUserByUsernameAndProject(String userName, String project);

    Optional<List<User>> findUserByEditorAndGroup(String editor, String group);
}

package fr.gouv.justice.mtteditique.repository;

import fr.gouv.justice.mtteditique.model.Ticket;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    @Query(value = "SELECT t FROM Ticket t INNER JOIN t.user u WHERE t.editor = :editor and t.project = :project and u.id = :userId and t.useCaseName in :useCaseName or (t.valideur.id =:userId and t.useCaseName in :useCaseName) ORDER BY t.updateDateTime DESC")
    Page<Ticket> getTickets(@Param("editor") String editor, @Param("project") String project, @Param("userId") Long userId, String[] useCaseName, Pageable pageable);

    Optional<Ticket> findTicketByDocumentId(String documentId);

    @Query(value = "SELECT t.id FROM Ticket t where t.documentId = :documentId")
    Long findTicketIdByDocumentId(@Param("documentId") String documentId);
}

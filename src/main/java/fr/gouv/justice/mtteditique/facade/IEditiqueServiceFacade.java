package fr.gouv.justice.mtteditique.facade;

import fr.gouv.justice.mtteditique.dto.DocumentSaveDTO;
import fr.gouv.justice.mtteditique.dto.WorkflowDTO;
import org.springframework.web.multipart.MultipartFile;

public interface IEditiqueServiceFacade {

    public abstract String authentication(String username, String project);

    public abstract String unitaryDocumentDownload(String token, String project, String flux);

    public abstract String interactivityDocumentEdit(String username, String token, String project, String useCaseName, MultipartFile file, Long group);

    public abstract Boolean interactivityDocumentSave(String token, DocumentSaveDTO documentSaveDTO, Long group);

    public abstract Boolean createWorkflow(String token, WorkflowDTO workflowDTO);

    public abstract String getTemplates(String token, String project);

}

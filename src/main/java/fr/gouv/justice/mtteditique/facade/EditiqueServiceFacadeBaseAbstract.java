package fr.gouv.justice.mtteditique.facade;

import org.springframework.http.HttpHeaders;

public abstract class EditiqueServiceFacadeBaseAbstract implements IEditiqueServiceFacade {

    protected EditiqueServiceFacadeBaseAbstract() {

    }

    /**
     * Headers
     *
     * @param token
     * @param contentType
     * @return
     */
    public HttpHeaders getHeader(String token, String contentType) {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", contentType);
        headers.add("Accept", "*/*");
        headers.add("Connection", "keep-alive");
        headers.add("httpheader", "true");
        headers.add("Authorization", token);

        return headers;
    }
}

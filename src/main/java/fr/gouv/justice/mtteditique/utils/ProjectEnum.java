package fr.gouv.justice.mtteditique.utils;

public enum ProjectEnum {

    PRISME("PRISME"),
    CASSIOPEE("CASSIOPEE"),
    PORTALIS("PORTALIS"),
    PPN("PPN");

    private final String project;

    ProjectEnum(String project) {
        this.project = project;
    }

    public String getProject() {
        return project;
    }
}

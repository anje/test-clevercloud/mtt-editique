package fr.gouv.justice.mtteditique.utils;

public enum StatusEnum {

    BROUILLON("BROUILLON"),
    TERMINER("TERMINER"),
    A_VALIDER("A_VALIDER"),
    VALIDER("VALIDER"),
    REJETER("REJETER"),
    ESCALADER("ESCALADER")
    ;

    private final String status;

    StatusEnum(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }
}

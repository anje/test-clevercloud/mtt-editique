package fr.gouv.justice.mtteditique.utils;

public enum ProviderEnum {

    OPENTEXT("OPENTEXT"),
    QUADIENT("QUADIENT");

    private final String provider;

    ProviderEnum(String provider) {
        this.provider = provider;
    }

    public String getProvider() {
        return provider;
    }
}

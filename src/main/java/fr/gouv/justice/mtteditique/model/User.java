package fr.gouv.justice.mtteditique.model;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "user")
public class User {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "username", nullable = false)
    private String username;

    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Column(name = "last_name", nullable = false)
    private String lastName;

    @Column(name = "password")
    private String password;

    @Column(name = "project", nullable = false)
    private String project;

    @Column(name = "civility", nullable = false)
    private String civility;

    @Column(name = "fonction", nullable = false)
    private String fonction;

    @Column(name = "group", nullable = false)
    private String group;

    @Column(name = "editor", nullable = false)
    private String editor;

    @Column(name = "is_contributor", nullable = false)
    private boolean isContributor;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<Ticket> tickets;
}
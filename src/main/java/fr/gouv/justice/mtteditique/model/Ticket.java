package fr.gouv.justice.mtteditique.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.Instant;
import java.util.List;

@Entity
@Data
@EntityListeners(AuditingEntityListener.class)
@Table(name = "ticket")
public class Ticket {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "document_id", nullable = false)
    private String documentId;

    @Column(name = "use_case_name", nullable = false)
    private String useCaseName;

    @Column(name = "url", nullable = false)
    private String url;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "editor", nullable = false)
    private String editor;

    @Column(name = "project", nullable = false)
    private String project;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "valideur_id")
    private User valideur;

    @Column(name = "op_url_workflow", nullable = false)
    private String opUrlWorkflow;

    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL)
    private List<Document> documents;

    @Column(name = "create_date_time", nullable = false, updatable = false)
    @CreatedDate
    private Instant createDateTime;

    @Column(name = "update_date_time", nullable = false)
    @LastModifiedDate
    private Instant updateDateTime;
}

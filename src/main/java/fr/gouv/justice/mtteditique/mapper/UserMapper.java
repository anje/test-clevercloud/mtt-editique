package fr.gouv.justice.mtteditique.mapper;

import fr.gouv.justice.mtteditique.dto.UserDTO;
import fr.gouv.justice.mtteditique.model.User;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {TicketMapper.class})
public interface UserMapper {

    List<UserDTO> toDTOs(List<User> users);

    UserDTO toDTO(User user);

    User toEntity(UserDTO userDTO);
}

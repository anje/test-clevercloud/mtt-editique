package fr.gouv.justice.mtteditique.mapper;

import fr.gouv.justice.mtteditique.dto.TicketDTO;
import fr.gouv.justice.mtteditique.model.Ticket;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {UserMapper.class, DocumentMapper.class})
public interface TicketMapper {

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "valideur.id", target = "valideurId")
    List<TicketDTO> toDTOs(List<Ticket> tickets);

    @Mapping(source = "user.id", target = "userId")
    @Mapping(source = "valideur.id", target = "valideurId")
    TicketDTO toDTO(Ticket ticket);

    @Mapping(source = "userId", target = "user.id")
    @Mapping(source = "valideurId", target = "valideur.id")
    Ticket toEntity(TicketDTO ticketDTO);
}

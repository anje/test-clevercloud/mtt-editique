package fr.gouv.justice.mtteditique.mapper;

import fr.gouv.justice.mtteditique.dto.DocumentDTO;
import fr.gouv.justice.mtteditique.model.Document;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", uses = {TicketMapper.class})
public interface DocumentMapper {

    @Mapping(source = "ticket.id", target = "ticketId")
    List<DocumentDTO> toDTOs(List<Document> documents);

    @Mapping(source = "ticket.id", target = "ticketId")
    DocumentDTO toDTO(Document document);

    @Mapping(source = "ticketId", target = "ticket.id")
    Document toEntity(DocumentDTO documentDTO);
}

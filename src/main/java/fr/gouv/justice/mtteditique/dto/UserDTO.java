package fr.gouv.justice.mtteditique.dto;

import lombok.Data;

@Data
public class UserDTO {

    private Long id;

    private String username;

    private String firstName;

    private String lastName;

    private String project;

    private String civility;

    private String fonction;

    private String group;

    private String editor;

    private boolean isContributor;
}

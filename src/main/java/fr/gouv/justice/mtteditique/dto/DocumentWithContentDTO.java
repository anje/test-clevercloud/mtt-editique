package fr.gouv.justice.mtteditique.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class DocumentWithContentDTO {

    private Long id;

    private Long size;

    private String filename;

    private String content;

    private String extension;

    private Long ticketId;

    private Instant createDateTime;

    private Instant updateDateTime;

}

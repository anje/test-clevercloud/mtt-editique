package fr.gouv.justice.mtteditique.dto;

import fr.gouv.justice.mtteditique.model.User;
import lombok.Data;
import java.time.Instant;
import java.util.Set;

@Data
public class TicketDTO {

    private Long id;

    private String documentId;

    private String useCaseName;

    private String url;

    private String status;

    private String editor;

    private String project;

    private Long userId;

    private Set<DocumentDTO> documents;

    private Long valideurId;

    private String opUrlWorkflow;

    private Instant createDateTime;

    private Instant updateDateTime;
}

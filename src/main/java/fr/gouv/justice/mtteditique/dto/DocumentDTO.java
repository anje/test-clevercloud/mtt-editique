package fr.gouv.justice.mtteditique.dto;

import lombok.Data;
import java.time.Instant;

@Data
public class DocumentDTO {

    private Long id;

    private Long size;

    private String filename;

    private String extension;

    private Long ticketId;

    private Instant createDateTime;

    private Instant updateDateTime;
}

package fr.gouv.justice.mtteditique.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class AuthRequestDTO {

    @NotBlank(message = "Editor may not be blank")
    private String editor;

    @NotBlank(message = "Project may not be blank")
    private String project;

    @NotBlank(message = "User may not be blank")
    private String user;
}

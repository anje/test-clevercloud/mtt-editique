package fr.gouv.justice.mtteditique.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class WorkflowDTO {

    private String documentID;

    private String appname;

    private String createur;

    private String valideur;

    private String status;

    private String editor;

    private Long priority;

    private String docName;

    private String dateEcheance;

}

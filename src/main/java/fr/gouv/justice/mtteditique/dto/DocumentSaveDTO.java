package fr.gouv.justice.mtteditique.dto;

import lombok.Data;

@Data
public class DocumentSaveDTO {

    private String url;

    private String documentId;

    private Long userId;

    private String filename;

    private String useCaseName;
}
